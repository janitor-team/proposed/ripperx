#ifndef main_data_h
#define main_data_h

const int MAX_NUM_TRACK = 100;
const int MAX_FILE_NAME_LENGTH = 512;

enum CommonEncoderType 
  {
    NO_TYPE = -1,
    CD,
    WAV,
    MP3,
    OGG,
    FLAC,
    MP2,
    MUSE
  }
  ;

class MainData 
{
  /**
   * Main data structure
   * All length is in cd sector unit
   **/
 public:
  enum Lengths
  {
    MAX_ARTIST_LENGTH = 1024,
    MAX_YEAR_LENGTH = 30
  };
  

  int total_length;
  char disc_artist[ MAX_ARTIST_LENGTH ];
  char disc_title[ MAX_ARTIST_LENGTH ];
  char disc_year[ MAX_YEAR_LENGTH ];
  char disc_category[ MAX_ARTIST_LENGTH ];
  struct Track
  {
    char title[ MAX_FILE_NAME_LENGTH ];
    unsigned begin;
    unsigned length;
    int make_wav;
    int wav_exist;
    int make_mp3;
    int mp3_exist;
    char *artist;
  } track[ MAX_NUM_TRACK ];
  int find_next_job(int cur_track, CommonEncoderType cur_type,
		    int *next_track, CommonEncoderType *next_type) const;
  void finish_job();
  
  MainData() 
    {
      num_tracks_ = 0;
    };
  
  void add_track() 
  {
    ++num_tracks_;
  };
  
  Track & back_track () 
    {
      return track[num_tracks_ - 1];
    };

  int num_tracks() const
  {
    return num_tracks_;
  }
  
  
 private:
  int num_tracks_;


};

#endif
