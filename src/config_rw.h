
#ifndef CONFIG_RW_H
#define CONFIG_RW_H

#include "common.h"
#include <vector>

class ConfigRW 
{
 public:  
  void read_config(void);
  void write_config(void);

  ConfigRW() 
    {
      init_config_data();
    }
  
  class ConfigRwInterface;

 private:
  
  typedef std::vector < std::pair<std::string,ConfigRwInterface*> > 
    config_rw_type;
  config_rw_type config_rw_data;

  template <class T, class U>
    void insert_pair(const char * label, T * dst, const U dft) ;
  
  void init_config_data(void);
};



#endif
